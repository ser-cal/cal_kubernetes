# Kubernetes Lab

## Pods und Services

In diesem Repo befinden sich erste, einfache Kubernetes Hands-on Übungen. Für die Durchführung verwenden wir zwei Docker-Images, welche wir vorher erstellt haben. Es handelt sich bei diesen Images um sehr simple **Node.js-Webanwendungen**. Falls Du wissen möchtest wie das geht, findest Du in meinem Repository [Container-WebApp_v1](https://gitlab.com/ser-cal/Container-CAL-webapp_v1) (Einfache Web-App in einen Container  verfrachten) weitere, sehr ausführliche Informationen:

**Vorsicht:** <br>
Dieses Image wird **nicht** gepflegt und beinhaltet allfällige "Vulnerabilities". Nutzung also auf **eigenes Risiko**

### Vorgehen
- Verbindung zwischen Kubernetes-Client und Server sicherstellen
- Repository klonen, um Übungen durchzuführen
- Deklaratives Pod-Manifest anwenden
- Deklaratives Service-Manifest anwenden
- Pod- & Service-Manifest via REST-API deployen
- Webdienst überprüfen
---
### Ausgangslage
Um Container-Images zu orchestrieren, muss auf dem Server "Kubernetes" installiert sein. Für dieses Hands-on wird dazu eine VM aus der TBZ-Cloud eingesetzt (Setup M300). Weitere Bedingungen sind in diesem Fall ein Host (Laptop) mit Gitbash, einer VPN-Verbindung zur TBZ-Cloud (Wireguard) und eine dedizierte VM. In diesem Fall handelt es sich um die VM mit der IP-Adresse **10.4.43.32**

### Verbindung zwischen Kubernetes-Client und Server aufbauen

Um vom Client (Laptop) aus Befehle an den API-Server (Kubernetes Control-Plane) zu senden, müssen auf der Clientseite folgende Bedingungen erfüllt sein:

- Das Programm **`kubectl`** muss installiert sein
- Das Config-File mit den entsprechenden Credentials muss entweder im **`.kube`-Verzeichnis** oder im entsprechenden Projektordner abgelegt sein

Die VM ist so aufgesetzt, dass man via Browser (Standard-Port 80) auf der Landing-Page landet:

> _Browser eigener Wahl öffnen und IP-Adresse des Kubernetes-Servers eingeben (`in diesem Fall "10.4.43.31" `)_<br>
> `1) ` _Link zu Config-file für den Kubernetes-Client (welches auf der Clientseite im **`.kube`-Verzeichnis** abgelegt wird)_<br>
> `2) ` _**[kubectl-Source-URL](https://kubernetes.io/de/docs/tasks/tools/install-kubectl/#installation-der-kubectl-anwendung-mit-curl)** mit Anleitung_<br>
  ![Screenshot](images/01_clientconfig.png) 

Wie weiter oben bereits erwähnt, kann das **`config`-file** auf der **Client-Seite** im Homeverzeichnis **`~/.kube`** oder im entsprechenden Projektordner abgelegt werden.

Auf dem Kubernetes-Server ist das file abgelegt unter **`/data/.ssh/config`** :

> `$ less /data/.ssh/config  `  _Inhalt des config-files anschauen_<br>
  ![Screenshot](images/03_clientconfig.png) 

---
Auf dem Kubernetes-Client kann das file unter **`<Benutzername>/.kube/config`** abgelegt werden :


  ![Screenshot](images/02_clientconfig.png) 

---

Git-Bash Fenster auf dem Client öffnen und überprüfen, ob die Verbindung zum Server steht


```
$ kubectl cluster-info
```

> `$ kubectl cluster-info  `  _Checken, ob Server (Control Plane) und Client miteinander sprechen können_<br>
  ![Screenshot](images/04_kubectl.png) 

---

### Repo klonen für Hands-on
Um die Übungen gleich selber Hands-on durchzuführen, empfiehlt es sich, dieses Repo mit sämtlichen Dateien runterzuladen

- Schritt 1. Pfad des Gitlab-Repositorys kopieren (gem. Bild unten)


  ![Screenshot](images/05_gitclone.png) 


- Schritt 2. Lokal in ein dafür geeignetes Verzeichnis wechseln und mit folgendem Kommando das Repository klonen:
```
$ git clone git@gitlab.com:ser-cal/cal_Kubernetes.git
```

> `$ git clone git@gitlab.com:ser-cal/cal_Kubernetes.git  `  _Repo in entsprechenes Verzeichnis klonen_<br>
  ![Screenshot](images/05b_gitclone.png) 

---

## Deklaratives Pod-Manifest anwenden
Um das Image "One" unter Kubernetes zu starten, deployen wir das **Pod-Manifest** **[calisto-pod.yaml](Pods/calisto-pod.yaml)**

Das Manifest kann nach eigenem Gutdünken angepasst werden. So kann z.B. unter "metadata" der Name des Pods oder auch der Label abgeändert werden. Beim Label ist darauf zu achten, dass dieser dann später mit dem "Label-Selector" des Services übereinstimmt. Andernfalls kann die App nicht über den Service angesprochen werden. Details dazu später.


```
## M. Calisto: getestet am 27.6.2021
## Beispiel Pod YAML:

apiVersion: v1
kind: Pod
metadata:
  name: calisto-pod
  labels: 
    app: web

spec:
  containers:
  - name: calisto-container
    image: marcellocalisto/webapp_one:1.0
    ports:
    - containerPort: 8080
```

Mit folgendem Befehl kann das Manifest an den API-Server (Kubernetes Control Plane) übergeben werden

```
$ kubectl apply -f calisto-pod.yaml
```

Anschliessend kontrollieren:

> `$ kubectl get pods --watch  `  _überprüfen, ob Pod läuft_<br>
  ![Screenshot](images/06_getpods.png) 





